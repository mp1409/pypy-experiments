This repository contains some experiments with PyPy and the RPython translation toolchain:
* The directory `NBody` contains a gravitational N-body simulation, with some benchmarking and graphing scripts.
* `RegisterMachine` contains an RPython interpreter for a simple register machine language (which can be translated to C and fitted with a JIT compiler).