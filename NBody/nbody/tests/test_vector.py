import unittest

from vector import Vector


class TimeoutTestCase(unittest.TestCase):
    def setUp(self):
        self.x = Vector([1.0, 2.0, 3.0, 4.0])
        self.y = Vector([5.0, 2.0, 4.0, 1.0])

    def test_add(self):
        z = self.x + self.y
        self.assertEqual(z._coords, [6.0, 4.0, 7.0, 5.0])

    def test_sub(self):
        z = self.x - self.y
        self.assertEqual(z._coords, [-4.0, 0.0, -1.0, 3.0])

    def test_scale(self):
        z = self.x.scale(10)
        self.assertEqual(z._coords, [10.0, 20.0, 30.0, 40.0])

    def test_dot(self):
        self.assertEqual(self.x.dot(self.y), 25)

    def test_abs(self):
        self.assertEqual(abs(self.x), 5.477225575051661)

    def test_abssub(self):
        self.assertEqual(abs(self.x - self.y), 5.0990195135927845)
