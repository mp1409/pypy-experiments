G = 6.674e-11


class Body(object):
    """ A class representing a celestial body."""

    def __init__(self, name, r, v, mass):
        """
        Construct a new body with a name, position r, velocity v and a mass.
        """
        self.name = name
        self.r = r
        self.v = v
        self.mass = mass

    def __str__(self):
        return '{} at {} with velocity {}'.format(self.name, self.r, self.v)

    def force_from(self, other):
        """ Return the (gravitational) force between this and another body. """
        delta = other.r - self.r
        dist = abs(delta)
        magnitude = (G * self.mass * other.mass) / (dist * dist)
        return delta.direction().scale(magnitude)

    def move(self, f, dt):
        """ Move the body by applying the force f for the time dt. """
        a = f.scale(1 / self.mass)
        self.v = self.v + (a.scale(dt))
        self.r = self.r + self.v.scale(dt)
