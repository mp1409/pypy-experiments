from __future__ import division, print_function

import json
import os
from time import clock

from universe import Universe


class Simulation(object):
    """
    Class representing a simulation run of an universe. There are three modes:

    In benchmarking mode, simply the overall execution time is printed as JSON.

    In tracing mode, the iteration steps are split into 1000 chunks, and for
    each chunk, the execution time is recorded and emitted as JSON.

    In graphing mode, the initial and final positions of the bodies will be
    printed, and a plot of the bodies traces is generated. This mode is intended
    for visual inspection of the simulation result.
    """

    def __init__(self, filename, iterations, mode, outputdir):
        """
        Initalize a simulation. The universe is loaded from filename, iterations
        specifies the number of steps, benchmark (True or False) sets the
        mode and outputdir sets the output directory.
        """

        self.name, _ = os.path.splitext(os.path.basename(filename))

        self.universe = Universe(filename)
        self.iterations = iterations

        self.mode = mode
        if self.mode == 'benchmark':
            self.perfdata = {}
        elif self.mode == 'trace':
            # Preallocated to save time while running.
            self.perfdata = {
                'iteration': [None] * 1001,
                'runtime': [None] * 1001
            }
        else:
            self.positions = []
            for body in self.universe.bodies:
                self.positions.append([[] for _ in range(len(body.r))])

        self.runtime = None
        self.outputdir = outputdir

    def run(self):
        """ Run the simulation. """
        iteration = 0

        def record_state():
            if self.mode == 'trace':
                chunk = iteration // (self.iterations // 1000)
                self.perfdata['iteration'][chunk] = iteration
                self.perfdata['runtime'][chunk] = clock()
            elif self.mode == 'graph':
                for body_idx in range(len(self.universe.bodies)):
                    for dim_idx in range(len(self.universe.bodies[body_idx].r)):
                        self.positions[body_idx][dim_idx].append(
                            self.universe.bodies[body_idx].r[dim_idx]
                        )

        starttime = clock()
        record_state()

        while iteration < self.iterations:
            self.universe.increase_time()
            iteration += 1

            if iteration % (self.iterations // 1000) == 0:
                record_state()

        stoptime = clock()
        self.runtime = stoptime - starttime

    def finalize(self):
        """ Depending on the mode, create plot or print performance data. """
        if self.mode == 'benchmark':
            self.perfdata['iterationspersec'] = self.iterations / self.runtime
            print(json.dumps(self.perfdata))
        elif self.mode == 'trace':
            print(json.dumps(self.perfdata))
        else:
            print('Finished, took {} seconds ({} iterations/second).'.format(
                self.runtime, self.iterations / self.runtime
            ))

            from matplotlib import colors, pyplot

            plot = pyplot.figure()
            ax = plot.add_subplot(1, 1, 1)

            m = self.universe.magnitude
            ax.set_xlim(-m, m)
            ax.set_ylim(-m, m)
            ax.grid(True)

            ax.set_xlabel('x (m)')
            ax.set_ylabel('y (m)')

            for bodypositions in self.positions:
                cm = colors.LinearSegmentedColormap.from_list(
                    'planets', ('0.7', '0')
                )
                xvals, yvals = bodypositions
                zvals = range(len(xvals))
                ax.scatter(xvals, yvals, s=3, c=zvals, cmap=cm)

            plot.savefig(os.path.join(
                self.outputdir,
                'Plots',
                'nbody_{}_{}.pdf'.format(self.name, self.iterations)
            ))
