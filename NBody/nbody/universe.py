from __future__ import print_function

import json

from body import Body
from vector import Vector


class Universe(object):
    """ Class representing a universe (consisting of multiple bodies). """

    def __init__(self, filename):
        """ Construct a new Universe object from filename. """

        with open(filename) as universefile:
            data = json.load(universefile)

        self.dimensions = data['dimensions']
        self.magnitude = data['magnitude']

        self.bodies = []
        for bodydata in data['bodies']:
            name = bodydata['name']
            r = Vector(bodydata['pos'])
            v = Vector(bodydata['velocity'])
            mass = bodydata['mass']
            self.bodies.append(Body(name, r, v, mass))

        self.dt = data['dt']

        self.iteration = 0

    def increase_time(self):
        """ Simulate the passing of dt seconds in the universe. """

        # Initialize the forces to zero:
        n = len(self.bodies)
        f = []
        for i in range(n):
            f.append(Vector([0, 0]))

        # Compute the forces:
        for i in range(n):
            for j in range(n):
                if i != j:
                    bodyi = self.bodies[i]
                    bodyj = self.bodies[j]
                    f[i] = f[i] + bodyi.force_from(bodyj)

        # Move the bodies.
        for i in range(n):
            self.bodies[i].move(f[i], self.dt)

        self.iteration += 1
