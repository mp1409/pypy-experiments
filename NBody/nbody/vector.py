import math


class Vector(object):
    """ A simple vector class for numeric Cartesian coordinates. """

    def __init__(self, iterable):
        """ Constructs a new vector from an iterable. """
        self._coords = list(iterable)

    def __len__(self):
        return len(self._coords)

    def __getitem__(self, i):
        return self._coords[i]

    def __iter__(self):
        for c in self._coords:
            yield c

    def __str__(self):
        return str(self._coords)

    def __add__(self, other):
        """ Returns the sum of the vector and another vector. """
        return Vector([a + b for a, b in zip(self, other)])

    def __sub__(self, other):
        """ Returns the difference of the vector and another vector. """
        return Vector([a - b for a, b in zip(self, other)])

    def scale(self, alpha):
        """ Returns the scaler product of the vector and scalar alpha. """
        return Vector([alpha * c for c in self])

    def dot(self, other):
        """ Returns the inner product of self and another vector. """
        result = 0
        for a, b in zip(self, other):
            result += a * b
        return result

    def __abs__(self):
        """ Return the magnitude (Euclidian norm) of the vector. """
        return math.sqrt(self.dot(self))

    def direction(self):
        """ Return the unit vector of the vector. """
        return self.scale(1.0 / abs(self))
