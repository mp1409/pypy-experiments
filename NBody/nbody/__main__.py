import argparse

from simulation import Simulation

parser = argparse.ArgumentParser(description='Gravitational n-body simulation')
parser.add_argument('universe', help='the file with the universe')
parser.add_argument('iterations', type=int, help='the number of iterations')
parser.add_argument(
    'mode', choices=('benchmark', 'trace', 'graph'),
    help='the mode of operation'
)
parser.add_argument('outputdir', help='the output directory')

args = parser.parse_args()
sim = Simulation(args.universe, args.iterations, args.mode, args.outputdir)
sim.run()
sim.finalize()
