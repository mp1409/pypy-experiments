from __future__ import print_function

import glob
import json
import math
from matplotlib import pyplot, ticker
import numpy
import os
import subprocess

OUTPUT_DIR = '../../Output'


def plot_sol_demo():
    subprocess.check_call((
        'python2', 'nbody', 'input/sol.json', str(int(2e4)), 'graph', OUTPUT_DIR
    ))


def plot_iterations_vs_time(rawdata):
    data = {}

    for ip in rawdata:
        data[ip] = {}

        for it in rawdata[ip][0]['iteration']:
            data[ip][it] = []

        for sample in rawdata[ip][2:]:
            for it, rt in zip(sample['iteration'], sample['runtime']):
                data[ip][it].append(rt)

    plot = pyplot.figure()
    ax = plot.add_subplot(1, 1, 1)
    max_time = 0
    max_iteration = 0

    i = 0
    for ip, style in zip(sorted(data), ('k-', 'k--', 'k-.')):
        iterations_list = []
        means_list = []
        std_list = []

        for it in sorted(data[ip]):
            iterations_list.append(it)
            means_list.append(numpy.mean(data[ip][it]))
            std_list.append(numpy.std(data[ip][it]))

        iterations = numpy.array(iterations_list)
        means = numpy.array(means_list)
        std = numpy.array(std_list)

        color = pyplot.cm.get_cmap('Vega10')(i)
        ax.plot(means, iterations, style, color=color, label=ip)
        ax.fill_betweenx(
            iterations, means - std, means + std, facecolor=color, alpha=0.4
        )

        max_time = max(max_time, numpy.amax(means + std))
        max_iteration = max(max_iteration, numpy.amax(iterations))

        i += 1

    ax.set_xlim(0, max_time * 1.05)
    ax.set_ylim(0, max_iteration)
    ax.grid(True)

    ax.annotate(
        'A', xy=(0.3, 30), xytext=(0.75, 150),
        bbox={'boxstyle': "circle", 'fc': 'none'},
        arrowprops={'arrowstyle': '->'}
    )

    ax.annotate(
        'B', xy=(0.4, 200), xytext=(0.15, 400),
        bbox={'boxstyle': "circle", 'fc': 'none'},
        arrowprops={'arrowstyle': '->'}
    )

    ax.set_xlabel('Process time (seconds)')
    ax.set_ylabel('Completed iterations')

    ax.legend(loc='lower right')

    plot.savefig(os.path.join(
        OUTPUT_DIR, 'Plots', 'nbody_iterations_vs_time.pdf')
    )


def plot_benchmarks(rawdata):
    output_path = os.path.join(OUTPUT_DIR, 'Plots', 'nbody_benchmark')

    data = {}

    plot = pyplot.figure()
    ax = plot.add_subplot(1, 1, 1)

    for ip in rawdata:
        data[ip] = numpy.array(
            [it['iterationspersec'] for it in rawdata[ip]][2:]
        )

    with open(output_path + '.txt', 'w') as logfile:
        for i, ip in enumerate(sorted(data)):
            logfile.write('{} mean {} it/sec, stddev {} it/sec\n'.format(
                ip, numpy.mean(data[ip]), numpy.std(data[ip])
            ))

            ax.violinplot(data[ip], (i, ), showmeans=True)

    ax.set_xticks(range(len(data)))
    ax.set_xticklabels(sorted(data))

    minval = min([min(a) for a in data.values()])
    maxval = max([max(a) for a in data.values()])

    ax.set_yscale('log')
    ax.yaxis.set_major_locator(ticker.LogLocator(subs='all'))
    ax.set_ylim(
        10 ** math.floor(math.log10(minval)),
        10 ** math.ceil(math.log10(maxval))
    )

    ax.grid(b=True)

    ax.set_ylabel('Iterations per Second')

    plot.savefig(output_path + '.pdf')


datafilepaths = glob.glob('../../Data/nbody_*.json')
datafilepath = sorted(datafilepaths)[-1]

with open(datafilepath, 'r') as datafile:
    results = json.load(datafile)

plot_sol_demo()
plot_iterations_vs_time(results['iterations_vs_time'])
plot_benchmarks(results['benchmark'])
