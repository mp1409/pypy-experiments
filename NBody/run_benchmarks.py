from __future__ import print_function

import datetime
import json
import os
import subprocess

CPYTHON_COMMAND = 'python2'
PYPY_COMMAND = 'pypy'
CPYTHON3_COMMAND = 'python3'
NUM_SAMPLES = 32
DATADIR = '../../Data'

interpreters = {}

# Sanity checks:
c = 'import platform as p; print p.python_implementation(), p.python_version()'
python_output = subprocess.check_output((CPYTHON_COMMAND, '-c', c))
if python_output.startswith('CPython 2.7.'):
    interpreters[CPYTHON_COMMAND] = python_output.strip()
else:
    raise RuntimeError('Wrong CPython version detected')

pypy_output = subprocess.check_output([PYPY_COMMAND, '-c', c])
if pypy_output.startswith('PyPy 2.7.'):
    interpreters[PYPY_COMMAND] = pypy_output.strip()
else:
    raise RuntimeError('Wrong PyPy version detected')

python3_output = subprocess.check_output((
    CPYTHON3_COMMAND, '-c',
    'import platform as p; print(p.python_implementation() + " " +'
    'p.python_version())'
))
if python3_output.startswith('CPython 3.6.'):
    interpreters[CPYTHON3_COMMAND] = python3_output.strip()
else:
    raise RuntimeError('Wrong CPython3 version detected')

# Save timestamp:
timestamp = datetime.datetime.now()

# Iterations vs Time:
results = {'iterations_vs_time': {}}
for cmd in interpreters:
    results['iterations_vs_time'][interpreters[cmd]] = []
    for _ in range(NUM_SAMPLES):
        output = subprocess.check_output((
            cmd, 'nbody', 'input/sol.json', str(int(2e3)), 'trace',
            '../../Output'
        ))
        results['iterations_vs_time'][interpreters[cmd]].append(
            json.loads(output)
        )
        print('.', end='')
print()

# Benchmarks:
results['benchmark'] = {}
for cmd in interpreters:
    results['benchmark'][interpreters[cmd]] = []
    for _ in range(NUM_SAMPLES):
        output = subprocess.check_output((
            cmd, 'nbody', 'input/sol.json', str(int(1e4)), 'benchmark',
            '../../Output'
        ))
        results['benchmark'][interpreters[cmd]].append(json.loads(output))
        print('.', end='')
print()

datafilepath = os.path.join(
    DATADIR, timestamp.strftime('nbody_%Y%m%d-%H%M%S.json')
)
with open(datafilepath, 'w') as datafile:
    json.dump(results, datafile)
