from __future__ import print_function

import datetime
import json
import os
import subprocess

NUM_SAMPLES = 32
DATADIR = '../../Data'
OUTPUT_DIR = '../../Output'
ARGS = [str(a) for a in ('slowsum.reg', 0, int(1e9), int(1e9))]

commands = {
    'Translated without JIT': ['./registermachine-nojit'],
    'Translated with JIT': ['./registermachine-jit']
}

timestamp = datetime.datetime.now()

results = {}

for name in commands:
    results[name] = []

    for _ in range(NUM_SAMPLES):
        output = subprocess.check_output(commands[name] + ARGS)
        results[name].append(float(output.splitlines()[1].split()[1]))
        print('.', end='')
    print()

datafilepath = os.path.join(
    DATADIR, timestamp.strftime('rm_%Y%m%d-%H%M%S.json')
)
with open(datafilepath, 'w') as datafile:
    json.dump(results, datafile)
