import time

try:
    from rpython.rlib.streamio import open_file_as_stream
    from rpython.rlib.jit import JitDriver
    from rpython.jit.codewriter.policy import JitPolicy
except ImportError:
    def open_file_as_stream(path, mode="r", buffering=-1):
        return open(path, mode, buffering)

    class JitDriver(object):
        def __init__(self, **kw): pass

        def jit_merge_point(self, **kw): pass

        def can_enter_jit(self, **kw): pass

    class JitPolicy(object):
        def __init__(self): pass

NUM_REGISTERS = 8

LOAD = 1
CLOAD = 2
STORE = 3
ADD = 4
CADD = 5
SUB = 6
CSUB = 7
GOTO = 8
IFZEROGOTO = 9
END = 10


def get_location(pc, _):
    return str(pc)

jitdriver = JitDriver(
    greens=['pc', 'program'], reds=['registers'],
    get_printable_location=get_location
)


def parse(filename):
    stream = open_file_as_stream(filename, 'r', 4096)

    program = []

    while True:
        line = stream.readline()
        if not line:
            break

        line = line.strip()

        if line[0] == '#':
            continue

        parts = line.split(' ')

        if parts[1] == 'LOAD':
            program.append((LOAD, int(parts[2])))
        elif parts[1] == 'CLOAD':
            program.append((CLOAD, int(parts[2])))
        elif parts[1] == 'STORE':
            program.append((STORE, int(parts[2])))         
        elif parts[1] == 'ADD':
            program.append((ADD, int(parts[2])))
        elif parts[1] == 'CADD':
            program.append((CADD, int(parts[2])))
        elif parts[1] == 'SUB':
            program.append((SUB, int(parts[2])))
        elif parts[1] == 'CSUB':
            program.append((CSUB, int(parts[2])))
        elif parts[1] == 'GOTO':
            program.append((GOTO, int(parts[2])))
        elif parts[1] == 'IFZEROGOTO':
            program.append((IFZEROGOTO, int(parts[2])))
        elif parts[1] == 'END':
            program.append((END, 42))
        else:
            raise RuntimeError('Parsing failed!')

    return program


def execute(program, initial):
    pc = 0
    registers = [0] * NUM_REGISTERS

    for i, arg in enumerate(initial):
        registers[i] = int(arg)
    
    while True:
        jitdriver.jit_merge_point(pc=pc, program=program, registers=registers)

        opcode, arg = program[pc]

        if opcode == LOAD:
            registers[0] = registers[arg]
            pc += 1
        elif opcode == CLOAD:
            registers[0] = arg
            pc += 1
        elif opcode == STORE:
            registers[arg] = registers[0]
            pc += 1
        elif opcode == ADD:
            registers[0] += registers[arg]
            pc += 1
        elif opcode == CADD:
            registers[0] += arg
            pc += 1
        elif opcode == SUB:
            registers[0] = max(registers[0] - registers[arg], 0)
            pc += 1
        elif opcode == CSUB:
            registers[0] = max(registers[0] - arg, 0)
            pc += 1
        elif opcode == GOTO:
            if arg < pc:
                jitdriver.can_enter_jit(
                    pc=pc, program=program, registers=registers
                )
            pc = arg
        elif opcode == IFZEROGOTO:
            if registers[0] == 0:
                if arg < pc:
                    jitdriver.can_enter_jit(
                        pc=pc, program=program, registers=registers
                    )
                pc = arg
            else:
                pc += 1
        elif opcode == END:
            print(registers)
            break


def jitpolicy(_):
    return JitPolicy()


def entry_point(argv):
    starttime = time.clock()

    program = parse(argv[1])
    execute(program, argv[2:])

    print('Runtime ' + str(time.clock() - starttime))
    return 0


def target(*_):
    return entry_point

if __name__ == '__main__':
    import sys
    entry_point(sys.argv)
