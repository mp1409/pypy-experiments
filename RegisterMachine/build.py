import subprocess

TRANSLATOR = 'pypy'

subprocess.check_call((
    TRANSLATOR, 'pypy2-v5.6.0-src/rpython/bin/rpython',
    '--output=registermachine-nojit', 'registermachine.py'
))

subprocess.check_call((
   TRANSLATOR, 'pypy2-v5.6.0-src/rpython/bin/rpython', '--opt=jit',
   '--output=registermachine-jit', 'registermachine.py'
))