from __future__ import print_function

import glob
import json
from matplotlib import pyplot
import numpy
import os

OUTPUT_DIR = '../../Output'

datafilepaths = glob.glob('../../Data/rm_*.json')
datafilepath = sorted(datafilepaths)[-1]

with open(datafilepath, 'r') as datafile:
    results = json.load(datafile)

plot = pyplot.figure()
ax = plot.add_subplot(1, 1, 1)

with open(os.path.join(OUTPUT_DIR, 'Plots', 'rm_benchmark.txt'), 'w') as f:
    for i, name in enumerate(sorted(results, reverse=True)):
        runtime = numpy.array(results[name][2:])

        f.write('{}: mean {}s, stddev {}s\n'.format(
            name, numpy.mean(runtime), numpy.std(runtime)
        ))

        ax.violinplot(runtime, (i, ), showmeans=True)

ax.set_xticks(range(len(results)))
ax.set_xticklabels(sorted(results, reverse=True))

ax.set_ylim(0, 1.05 * max([max(a) for a in results.values()]))

ax.grid(b=True)
ax.set_ylabel('Process time (seconds)')

plot.savefig(os.path.join(OUTPUT_DIR, 'Plots', 'rm_benchmark.pdf'))
